function cm(m::Int, omega::Float64, filter_type::Int)
    @assert filter_type >= 1
    @assert filter_type <= 4
    if filter_type == 1
        if m == 0
            return 1
        else
            return 2*cos(omega*m)
        end
    elseif filter_type == 2
        return 2*cos(omega*(m+1/2))
    elseif filter_type == 3
        return 2*sin(omega*(m-1))
    elseif filter_type == 4
        return 2*sin(omega*(m+1/2))
    end
end

"""
    fir_design!(model::Model,
                specifications::Vector{Tuple{Float64, Float64, Float64}},
                wordlength::Int,
                ;verbose::Bool = false)

Modify `model` to add fir specific constraints according to `specifications`.
"""
function fir_design!(
        model::Model,
        specifications::Vector{Tuple{Float64, Float64, Float64}},
        wordlength::Int,
        ;filter_order::Int,
        filter_type::Int,
        verbose::Bool=false,
        fixed_gain::Bool=true,
        errorbound::Int=0,
        wordlength_in::Int,
        step_presolve::Bool = false,
        simplify_cost::Bool = false,
        min_gain::Float64=0.01,
        kwargs...
    )
    @assert mod(filter_order, 2) == mod(filter_type+1, 2)
    model[:ToLink] = Vector{VariableRef}()
    model[:ToLinkIsZero] = Vector{VariableRef}()
    model[:ToLinkError] = Vector{VariableRef}()
    model[:ToLinkMSB] = Vector{VariableRef}()
    model[:ToLinkZeros] = Vector{VariableRef}()
    model[:ToLinkOBA] = Vector{VariableRef}()

    # Ref Table I: 10.1109/TCAD.2022.3179221
    nb_coeffs = div(filter_order + mod(filter_type+1, 2), 2) + (filter_type == 1)
    verbose && println("Generation of FIR design part, nb_coeffs: $(nb_coeffs)")

    minvalue = -2^(wordlength-1)
    maxvalue = 2^(wordlength-1)-1
    maximum_value = max(abs(minvalue), abs(maxvalue))
    @variable(model, gain)
    if fixed_gain
        fix(gain, 1.0, force=true)
    else
        set_lower_bound(gain, min_gain)
    end
    @variable(model, minvalue <= h[0:(nb_coeffs-1)] <= maxvalue, Int)
    if !step_presolve
        @variable(model, hsign[0:(nb_coeffs-1)], Bin)
        @variable(model, 0 <= hpos[0:(nb_coeffs-1)] <= maximum_value, Int)
        @variable(model, 0 <= hposodd[0:(nb_coeffs-1)] <= maximum_value, Int)
    end

    # Filter fits specifications
    @constraint(model, [i in 1:length(specifications)], 2^(wordlength-1)*gain*specifications[i][2] <= sum(h[j]*cm(j, specifications[i][1], filter_type) for j in 0:(nb_coeffs-1)))
    @constraint(model, [i in 1:length(specifications)], sum(h[j]*cm(j, specifications[i][1], filter_type) for j in 0:(nb_coeffs-1)) <= gain*specifications[i][3]*2^(wordlength-1))

    if !step_presolve
        # Get coefficient sign
        @constraint(model, [j in 0:(nb_coeffs-1)], h[j] >= minvalue*(1-hsign[j]))
        @constraint(model, [j in 0:(nb_coeffs-1)], h[j] <= maxvalue*hsign[j])

        # Compute abs(h)
        @constraint(model, [j in 0:(nb_coeffs-1)], hpos[j] >= h[j])
        @constraint(model, [j in 0:(nb_coeffs-1)], hpos[j] <= h[j] + (maxvalue-minvalue)*(1-hsign[j]))
        @constraint(model, [j in 0:(nb_coeffs-1)], hpos[j] >= -h[j])
        @constraint(model, [j in 0:(nb_coeffs-1)], hpos[j] <= -h[j] + (maxvalue-minvalue)*hsign[j])

        # Get odd part of coefficients
        @variable(model, hPsias[0:(nb_coeffs-1), -(wordlength-1):0], Bin)
        @constraint(model, [j in 0:(nb_coeffs-1)], sum(hPsias[j,s] for s in -(wordlength-1):0) == 1)
        @constraint(model, [j in 0:(nb_coeffs-1), s in -(wordlength-1):0], hpos[j] >= 2^(-s)*hposodd[j] + (hPsias[j,s] - 1)*(maximum_value*(2^(-s))))
        @constraint(model, [j in 0:(nb_coeffs-1), s in -(wordlength-1):0], hpos[j] <= 2^(-s)*hposodd[j] + (1 - hPsias[j,s])*(maximum_value*(2^(-s))))
        @variable(model, 0 <= hforce_odd[0:(nb_coeffs-1)] <= maximum_value, Int)
        @variable(model, h_is_zero[0:(nb_coeffs-1)], Bin)
        @constraint(model, [j in 0:(nb_coeffs-1)], hposodd[j] == 2*hforce_odd[j]+1-h_is_zero[j])
        @constraint(model, [j in 0:(nb_coeffs-1)], hpos[j] <= maximum_value*(1-h_is_zero[j]))

        # For link with MCM:
        model[:ToLink] = Vector{VariableRef}(hposodd)
        model[:ToLinkIsZero] = Vector{VariableRef}(h_is_zero)

        # Include error analysis
        @variable(model, 0 <= error_outputs[0:(nb_coeffs-1)] <= errorbound, Int)
        @variable(model, 0 <= error_in_structadder[1:filter_order, 1:2] <= errorbound, Int)
        @constraint(model, error_in_structadder[1, 1] == error_outputs[end])
        @constraint(model, [J in 0:(nb_coeffs-1)], error_in_structadder[filter_order-nb_coeffs+J+1, 2] == error_outputs[J])
        @constraint(model, [J in 1:(nb_coeffs-2)], error_in_structadder[J, 2] == error_outputs[nb_coeffs-J-1])

        max_wordlength = round(Int, log2((2^wordlength - 1)*(2^wordlength_in)), RoundUp) + nb_coeffs

        @variable(model, 0 <= error_out_structadder[1:filter_order] <= errorbound, Int)
        @variable(model, 0 <= error_out <= errorbound, Int)
        if !simplify_cost
            @variable(model, 0 <= ti_structadder[1:filter_order, 1:2] <= max_wordlength, Int)
            @variable(model, 0 <= tmax_structadder[1:filter_order] <= max_wordlength, Int)
            @variable(model, tib_structadder[1:filter_order, 1:2, 0:max_wordlength], Bin)
            @variable(model, 0 <= error_ti_structadder[1:filter_order, 1:2] <= 2^max_wordlength, Int)
            @variable(model, 0 <= error_Ti_structadder[1:filter_order, 1:2] <= 2^max_wordlength, Int)
            @variable(model, 0 <= error_Zi_structadder[1:filter_order, 1:2] <= 2^max_wordlength, Int)
            @variable(model, tB_structadder[1:filter_order], Bin)
            @constraint(model, [J in 1:filter_order], tmax_structadder[J] <= ti_structadder[J,1] + max_wordlength*(1-tB_structadder[J]))
            @constraint(model, [J in 1:filter_order], tmax_structadder[J] <= ti_structadder[J,2] + max_wordlength*(tB_structadder[J]))

            @constraint(model, [J in 2:filter_order], error_in_structadder[J, 1] == error_out_structadder[J-1])
            @constraint(model, [J in 1:filter_order], error_out_structadder[J] == error_in_structadder[J, 1] + error_in_structadder[J, 2] + error_ti_structadder[J, 1] + error_ti_structadder[J, 2])
            @constraint(model, error_out == error_out_structadder[filter_order])
            @constraint(model, error_out <= errorbound)
        else
            @constraint(model, [J in 2:filter_order], error_in_structadder[J, 1] == error_out_structadder[J-1])
            @constraint(model, [J in 1:filter_order], error_out_structadder[J] == error_in_structadder[J, 1] + error_in_structadder[J, 2])
            @constraint(model, error_out == error_out_structadder[filter_order])
            @constraint(model, error_out <= errorbound)
        end

        # For link with outputs MCM
        model[:ToLinkError] = Vector{VariableRef}(error_outputs)

        # Include one-bit adder count analysis
        # MSB
        if !simplify_cost
            @variable(model, 0 <= msb_outputs[0:(nb_coeffs-1)] <= max_wordlength, Int)
            @variable(model, 0 <= msb_outputs_shifted[0:(nb_coeffs-1)] <= max_wordlength, Int)
            @constraint(model, [J in 0:(nb_coeffs-1)], msb_outputs_shifted[J] == msb_outputs[J] + sum(-s*hPsias[J,s] for s in -(wordlength-1):0))
            @variable(model, 0 <= msb_in_structadder[1:filter_order, 1:2] <= max_wordlength, Int)
            @constraint(model, msb_in_structadder[1, 1] == msb_outputs_shifted[end])

            @constraint(model, [J in 0:(nb_coeffs-1)], msb_in_structadder[filter_order-nb_coeffs+J+1, 2] ==  msb_outputs_shifted[J])
            @constraint(model, [J in 1:(nb_coeffs-2)], msb_in_structadder[J, 2] == msb_outputs_shifted[nb_coeffs-J-1])

            @variable(model, 0 <= msb_out_structadder[1:filter_order] <= max_wordlength, Int)
            @constraint(model, [J in 2:filter_order], msb_in_structadder[J, 1] == msb_out_structadder[J-1])
            @variable(model, msb_bin[1:filter_order], Bin)
            @variable(model, msbincrease[1:filter_order], Bin)
            @constraint(model, [J in 1:filter_order], msb_out_structadder[J] >= msb_in_structadder[J, 1] + msbincrease[J])
            @constraint(model, [J in 1:filter_order], msb_out_structadder[J] >= msb_in_structadder[J, 2] + msbincrease[J])
            @constraint(model, [J in 1:filter_order], msb_out_structadder[J] <= msb_in_structadder[J, 1] + 1 + max_wordlength*(1-msb_bin[J]) + 1) # +1 because of possible error
            @constraint(model, [J in 1:filter_order], msb_out_structadder[J] <= msb_in_structadder[J, 2] + 1 + max_wordlength*(msb_bin[J]) + 1) # +1 because of possible error

            @variable(model, 0 <= Hbound[0:filter_order] <= 2^max_wordlength, Int)
            xabsmax = 2^wordlength_in
            @constraint(model, Hbound[0] == hpos[end]*xabsmax)
            @constraint(model, [J in 0:(nb_coeffs-1)], Hbound[filter_order-nb_coeffs+J+1] == Hbound[filter_order-nb_coeffs+J+1-1] + hpos[J]*xabsmax)
            @constraint(model, [J in 1:(nb_coeffs-2)], Hbound[J] == Hbound[J-1] + hpos[nb_coeffs-J-1]*xabsmax)

            @variable(model, msbab_struct[1:filter_order, 1:max_wordlength], Int)
            # C5.32 #issue with inttol hence indicator constraint
            @constraint(model, [J in 1:filter_order, t in 1:max_wordlength], 2^(t+1)-1 >= Hbound[J] + error_out_structadder[J] - (1 - msbab_struct[J, t])*2^max_wordlength)
            @constraint(model, [J in 1:filter_order, t in 1:max_wordlength], msbab_struct[J, t] --> {2^(t+1)-1 >= Hbound[J] + error_out_structadder[J]})
            # C5.33
            @constraint(model, [J in 1:filter_order], sum(msbab_struct[J, t] for t in 1:max_wordlength) == 1)
            # C5.34
            @constraint(model, [J in 1:filter_order], sum(t*msbab_struct[J, t] for t in 1:max_wordlength) == msb_out_structadder[J])


            @variable(model, is_subtraction[1:filter_order, 1:2], Bin)
            # Zeros
            @variable(model, 0 <= nbzeros_outputs[0:(nb_coeffs-1)] <= max_wordlength, Int)
            @variable(model, 0 <= nbzeros_outputs_shifted[0:(nb_coeffs-1)] <= max_wordlength, Int)
            @constraint(model, [J in 0:(nb_coeffs-1)], nbzeros_outputs_shifted[J] == nbzeros_outputs[J] + sum(-s*hPsias[J,s] for s in -(wordlength-1):0))
            @variable(model, 0 <= nbzeros_in_structadder[1:filter_order, 1:2] <= max_wordlength, Int)
            @variable(model, nbzerosb_in_structadder[1:filter_order, 1:2, 0:max_wordlength], Bin)

            @constraint(model, nbzeros_in_structadder[1, 1] == nbzeros_outputs_shifted[end])
            @constraint(model, [J in 0:(nb_coeffs-1)], nbzeros_in_structadder[filter_order-nb_coeffs+J+1, 2] ==  nbzeros_outputs_shifted[J])
            @constraint(model, [J in 1:(nb_coeffs-2)], nbzeros_in_structadder[J, 2] == nbzeros_outputs_shifted[nb_coeffs-J-1])



            @constraint(model, [J in 1:filter_order], ti_structadder[J,1] <= nbzeros_in_structadder[J,1] + max_wordlength*(1-is_subtraction[J,2]))
            @constraint(model, [J in 1:filter_order], ti_structadder[J,2] <= nbzeros_in_structadder[J,2] + max_wordlength*(1-is_subtraction[J,1]))
            @constraint(model, [J in 1:filter_order, i in 1:2], ti_structadder[J,i] >= nbzeros_in_structadder[J,i])

            @constraint(model, [J in 1:filter_order, i in 1:2], sum(tib_structadder[J,i,b] for b in 0:max_wordlength) == 1)
            @constraint(model, [J in 1:filter_order, i in 1:2], sum(nbzerosb_in_structadder[J,i,b] for b in 0:max_wordlength) == 1)
            @constraint(model, [J in 1:filter_order, i in 1:2], sum(b*tib_structadder[J,i,b] for b in 0:max_wordlength) == ti_structadder[J,i])
            @constraint(model, [J in 1:filter_order, i in 1:2], sum(b*nbzerosb_in_structadder[J,i,b] for b in 0:max_wordlength) == nbzeros_in_structadder[J,i])
            @constraint(model, [J in 1:filter_order, i in 1:2, b in 1:max_wordlength], error_Ti_structadder[J,i] >= 2^(b)-1 - 2^max_wordlength*(1-tib_structadder[J,i,b]))
            @constraint(model, [J in 1:filter_order, i in 1:2, b in 1:max_wordlength], error_Zi_structadder[J,i] <= 2^(b)-1 + 2^max_wordlength*(1-nbzerosb_in_structadder[J,i,b]))
            @constraint(model, [J in 1:filter_order, i in 1:2], error_Zi_structadder[J,i] <= 2^max_wordlength*(1-nbzerosb_in_structadder[J,i,0]))
            @constraint(model, [J in 1:filter_order, i in 1:2], error_ti_structadder[J,i] >= error_Ti_structadder[J,i] - error_Zi_structadder[J,i])

            @constraint(model, [J in 0:(nb_coeffs-1), i in 1:2], error_ti_structadder[filter_order-nb_coeffs+J+1,i] <= (1-h_is_zero[J])*2^max_wordlength)
            @constraint(model, [J in 1:(nb_coeffs-2), i in 1:2], error_ti_structadder[J,i] <= (1-h_is_zero[nb_coeffs-J-1])*2^max_wordlength)


            @variable(model, 0 <= nbzeros_out_structadder[1:filter_order] <= max_wordlength, Int)
            @constraint(model, [J in 2:filter_order], nbzeros_in_structadder[J, 1] == nbzeros_out_structadder[J-1])
            @constraint(model, [J in 1:filter_order], nbzeros_out_structadder[J] <= ti_structadder[J, 1])
            @constraint(model, [J in 1:filter_order], nbzeros_out_structadder[J] <= ti_structadder[J, 2])
            @constraint(model, [J in 1:filter_order], nbzeros_out_structadder[J] <= max_wordlength*is_subtraction[J, 1])
            @constraint(model, [J in 1:filter_order], nbzeros_out_structadder[J] <= max_wordlength*is_subtraction[J, 2])

            @constraint(model, [J in 2:filter_order], is_subtraction[J, 1] == 0)
            if filter_type == 1
                @constraint(model, is_subtraction[1, 1] == 1 - hsign[end])
                @constraint(model, [J in 0:(nb_coeffs-1)], is_subtraction[filter_order-nb_coeffs+J+1, 2] == 1 - hsign[J])
                @constraint(model, [J in 1:(nb_coeffs-2)], is_subtraction[J, 2] == 1 - hsign[nb_coeffs-J-1])
            elseif filter_type == 2
                @constraint(model, is_subtraction[1, 1] == 1 - hsign[end])
                @constraint(model, [J in 0:(nb_coeffs-1)], is_subtraction[nb_coeffs+J, 2] == 1 - hsign[J])
                @constraint(model, [J in 1:(nb_coeffs-2)], is_subtraction[J, 2] == 1 - hsign[nb_coeffs-J])
            elseif filter_type == 3
                @constraint(model, [J in nb_coeffs:filter_order], is_subtraction[J, 2] == hsign[filter_order-J])
            elseif filter_type == 4
                @constraint(model, [J in nb_coeffs:filter_order], is_subtraction[J, 2] == hsign[filter_order-J])
            end

            @variable(model, 0 <= oba_struct_cost[1:filter_order] <= max_wordlength, Int)
            @variable(model, nbz_bin[1:filter_order], Bin)
            @constraint(model, [J in 1:filter_order], oba_struct_cost[J] >= msb_out_structadder[J]+1-msbincrease[J] - ti_structadder[J, 1] - nbz_bin[J]*max_wordlength)
            @constraint(model, [J in 1:filter_order], oba_struct_cost[J] >= msb_out_structadder[J]+1-msbincrease[J] - ti_structadder[J, 2] - (1-nbz_bin[J])*max_wordlength)
            @constraint(model, [J in 1:filter_order], oba_struct_cost[J] >= msb_out_structadder[J]+1-msbincrease[J] - (1-is_subtraction[J, 1])*max_wordlength)
            @constraint(model, [J in 1:filter_order], oba_struct_cost[J] >= msb_out_structadder[J]+1-msbincrease[J] - (1-is_subtraction[J, 2])*max_wordlength)

            @variable(model, 0 <= oba_struct_cost_nozeros[1:filter_order] <= max_wordlength, Int)
            @constraint(model, [J in 0:(nb_coeffs-1)], oba_struct_cost_nozeros[filter_order-nb_coeffs+J+1] >= oba_struct_cost[filter_order-nb_coeffs+J+1] - h_is_zero[J]*max_wordlength)
            @constraint(model, [J in 1:(nb_coeffs-2)], oba_struct_cost_nozeros[J] >= oba_struct_cost[J] - h_is_zero[nb_coeffs-J-1]*max_wordlength)
            @constraint(model, [J in 0:(nb_coeffs-1)], oba_struct_cost_nozeros[filter_order-nb_coeffs+J+1] <= (1-h_is_zero[J])*max_wordlength)
            @constraint(model, [J in 1:(nb_coeffs-2)], oba_struct_cost_nozeros[J] <= (1-h_is_zero[nb_coeffs-J-1])*max_wordlength)

            # For link with outputs MCM
            model[:ToLinkMSB] = Vector{VariableRef}(msb_outputs)
            model[:ToLinkZeros] = Vector{VariableRef}(nbzeros_outputs)
            # For link in obj function
            model[:ToLinkOBA] = Vector{VariableRef}(oba_struct_cost_nozeros)
        end
    end

    return model
end
