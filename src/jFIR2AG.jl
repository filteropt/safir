module jFIR2AG

using JuMP
using AdderGraphs

mutable struct Fir2AdderGraphs
    coefficients::Vector{Int}
    truncations::Vector{Tuple{Int, Int}} # from AG, then from struct
    gain::Float64
    addergraph::AdderGraph
end

function Fir2AdderGraphs()
    return Fir2AdderGraphs(Vector{Int}(), Vector{Tuple{Int, Int}}(), 1.0, AdderGraph(Vector{AdderNode}()))
end

function Fir2AdderGraphs(
        coefficients::Vector{Int},
        gain::Float64
    )
    return Fir2AdderGraphs(coefficients, Vector{Tuple{Int, Int}}(), gain, AdderGraph(Vector{AdderNode}()))
end

# Includes
include("utils.jl")
include("rpag.jl")
include("fir_ilp1.jl")
include("fir.jl")
include("mcm_ilp1.jl")
include("presolve.jl")
include("design.jl")

export Fir2AdderGraphs
export design_fir
export get_specifications
export isempty

end # module
