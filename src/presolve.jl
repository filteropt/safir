function presolve!(model::Model; verbose::Bool=false, verbose_presolve::Bool=false, timelimit_presolve::Int=0, warmstart_coeffs::Vector{Int}=Vector{Int}(), kwargs...)
    time_limit_model = time_limit_sec(model)
    unset_time_limit_sec(model)
    verbose_presolve && println("No objective presolve")
    !verbose_presolve && set_silent(model)
    model[:infeasibility_proven] = false
    model[:start_values_h] = warmstart_coeffs
    if isempty(warmstart_coeffs)
        optimize!(model)
        if !has_values(model)
            if termination_status(model) == MOI.INFEASIBLE
                model[:infeasibility_proven] = true
            end
            unset_silent(model)
            return model
        end

        model[:start_values_h] = collect(round.(Int, value.(model[:h])))
    end

    for j in 0:(length(model[:h])-1)
        set_start_value(model[:h][j], model[:start_values_h][j+1])
    end

    if timelimit_presolve >= 1
        set_time_limit_sec(model, timelimit_presolve)
    end

    model[:bounds] = Vector{Tuple{Int, Int}}()
    for j in 0:(length(model[:h])-1)
        @objective(model, Min, model[:h][j])
        optimize!(model)
        h_lower_bound = lower_bound(model[:h][j])
        if termination_status(model) == MOI.OPTIMAL
            h_lower_bound = round(Int, value(model[:h][j]))
        else
            h_lower_bound = round(Int, objective_bound(model))
        end
        set_lower_bound(model[:h][j], h_lower_bound)
        @objective(model, Max, model[:h][j])
        optimize!(model)
        h_upper_bound = upper_bound(model[:h][j])
        if termination_status(model) == MOI.OPTIMAL
            h_upper_bound = round(Int, value(model[:h][j]))
        else
            h_upper_bound = round(Int, objective_bound(model))
        end
        set_upper_bound(model[:h][j], h_upper_bound)
        push!(model[:bounds], (h_lower_bound, h_upper_bound))
        verbose && println("\tmodel[:h][$j] in ", (h_lower_bound, h_upper_bound))
    end
    unset_silent(model)
    unset_time_limit_sec(model)
    if !isnothing(time_limit_model)
        set_time_limit_sec(model, time_limit_model)
    end
    return model
end
