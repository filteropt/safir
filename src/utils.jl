# function odd(number::Int)
#     if number == 0
#         return 0
#     end
#     while mod(number, 2) == 0
#         number = div(number, 2)
#     end
#     return number
# end

import Base.isempty
function isempty(solution::Fir2AdderGraphs)
    return isempty(solution.coefficients)
end


"""
    fix_to_bounds!(model::Model,
                   bounds::Vector{Tuple{Int, Int}})

Fix variables h_k to given bounds.
"""
function fix_to_bounds!(model::Model, bounds::Vector{Tuple{Int, Int}})
    for j in 0:(length(model[:h])-1)
        h_lower_bound, h_upper_bound = bounds[j+1]
        set_lower_bound(model[:h][j], h_lower_bound)
        set_upper_bound(model[:h][j], h_upper_bound)
        set_lower_bound(model[:hpos][j], max(h_lower_bound, 0))
        set_upper_bound(model[:hpos][j], max(abs(h_lower_bound), abs(h_upper_bound)))
        set_upper_bound(model[:hposodd][j], max(abs(h_lower_bound), abs(h_upper_bound)))
    end
    return model
end


function fix_start!(model::Model, start_values_h::Vector{Int}; fixed_ws::Bool=false, kwargs...)
    for j in 0:(length(model[:h])-1)
        set_start_value(model[:h][j], start_values_h[j+1])
        set_start_value(model[:hpos][j], abs(start_values_h[j+1]))
        set_start_value(model[:hposodd][j], odd(abs(start_values_h[j+1])))
        set_start_value(model[:hsign][j], Int(start_values_h[j+1] >= 0))
        set_start_value(model[:h_is_zero][j], Int(start_values_h[j+1] == 0))
    end
    if fixed_ws
        for j in 0:(length(model[:h])-1)
            set_lower_bound(model[:h][j], start_values_h[j+1])
            set_upper_bound(model[:h][j], start_values_h[j+1])
            set_lower_bound(model[:hpos][j], abs(start_values_h[j+1]))
            set_upper_bound(model[:hpos][j], abs(start_values_h[j+1]))
            set_lower_bound(model[:hposodd][j], odd(abs(start_values_h[j+1])))
            set_upper_bound(model[:hposodd][j], odd(abs(start_values_h[j+1])))
            set_lower_bound(model[:hsign][j], Int(start_values_h[j+1] >= 0))
            set_upper_bound(model[:hsign][j], Int(start_values_h[j+1] >= 0))
            set_lower_bound(model[:h_is_zero][j], Int(start_values_h[j+1] == 0))
            set_upper_bound(model[:h_is_zero][j], Int(start_values_h[j+1] == 0))
        end
    end
    return model
end



"""
    get_specifications(fbands::Vector{Tuple{Float64, Float64}},
                       abands::Vector{Tuple{Float64, Float64}})

Return specifications on a uniform grid from `fbands` and `abands`.
"""
function get_specifications(fbands::Vector{Tuple{Float64, Float64}},
                            abands::Vector{Tuple{Float64, Float64}},
                            size_of_grid::Int=480)
    specifications = Vector{Tuple{Float64, Float64, Float64}}()
    total_band_size = 0
    for band in fbands
        total_band_size += band[2]-band[1]
    end
    for i in 1:length(fbands)
        band = fbands[i]
        size_of_local_grid = ceil(Int, size_of_grid*(band[2]-band[1])/total_band_size)
        for omega in 0:size_of_local_grid
            push!(specifications, (pi*(band[1]+(band[2]-band[1])*omega/max(size_of_local_grid,1)), abands[i][1], abands[i][2]))
        end
    end
    return specifications
end

get_specifications(fbands::Vector{Tuple{Float64, Float64}},
                   dbands::Union{Vector{Float64},Vector{Int}},
                   deltas::Vector{Float64},
                   size_of_grid::Int) = get_specifications(fbands, [(dbands[i]-deltas[i], dbands[i]+deltas[i]) for i in 1:length(fbands)], size_of_grid)

get_specifications(fbands::Vector{Tuple{Float64, Float64}},
                   dbands::Union{Vector{Float64},Vector{Int}},
                   delta::Float64,
                   size_of_grid::Int) = get_specifications(fbands, dbands, fill(delta, length(dbands)), size_of_grid)


function get_specifications(transfer_function::Function,
                            error_margin_percent::Union{Float64, Int},
                            error_minimum::Float64,
                            size_of_grid::Int)
    specifications::Vector{Tuple{Float64, Float64, Float64}} = [(pi*x/size_of_grid, (transfer_function(x/size_of_grid)*(1-error_margin_percent/100) < error_minimum ? 0.0 : transfer_function(x/size_of_grid)*(1-error_margin_percent/100)), max(transfer_function(x/size_of_grid)*(1+error_margin_percent/100),error_minimum)) for x in 0:size_of_grid if !isnan(transfer_function(x/size_of_grid))]
    return specifications
end
