function model_mcm_forumlation!(model::Model, C::Vector{VariableRef},
                                is_zero_C::Vector{VariableRef},
                                epsilon_C::Vector{VariableRef},
                                msb_C::Vector{VariableRef},
                                zeros_C::Vector{VariableRef},
                                oba_C::Vector{VariableRef},
                                wordlength::Int,
                                NA::Int;
                                verbose::Bool=false,
                                known_min_NA::Int=0,
                                addergraph_warmstart::AdderGraph=AdderGraph(),
                                start_values_h::Vector{Int}=Vector{Int}(),
                                wordlength_in::Int,
                                errorbound::Int=0,
                                simplify_cost::Bool=false,
                                kwargs...
    )::Model

    # Prepare warmstart
    if !isempty(addergraph_warmstart)
        addernodes = get_nodes(addergraph_warmstart)
        for addernode in addernodes
            sort!(addernode.inputs, by=x->x.shift, rev=true)
        end
        output_values = get_outputs(addergraph_warmstart)
        addernodes_value_to_index = Dict{Int, Int}([get_value(addernodes[i]) => i for i in 1:length(addernodes)])
        addernodes_value_to_index[1] = 0
        NA = length(addernodes)
    end

    #
    Smin, Smax = -wordlength, wordlength
    NO = length(C)
    verbose && println("\tBounds on the number of adder: $(known_min_NA)--$(NA)")
    maximum_target = max(maximum(upper_bound.(C)), -minimum(lower_bound.(C)))
    maximum_value = 2^wordlength
    known_min_NA = min(NA, known_min_NA)

    @variable(model, 1 <= ca[0:NA] <= maximum_value-1, Int)
    @constraint(model, [a in 1:known_min_NA], ca[a] >= 3)
    @variable(model, 1 <= ca_no_shift[1:NA] <= maximum_value*2, Int)
    @variable(model, 1 <= cai[1:NA, 1:2] <= maximum_value-1, Int)
    @variable(model, 1 <= cai_left_sh[1:NA] <= maximum_value*2, Int)
    @variable(model, -2*maximum_value <= cai_left_shsg[1:NA] <= maximum_value*2, Int)
    @variable(model, -2*maximum_value <= cai_right_sg[1:NA] <= maximum_value*2, Int)

    @variable(model, Phiai[1:NA, 1:2], Bin)
    @variable(model, caik[a in 1:NA, 1:2, 0:(a-1)], Bin)
    @variable(model, phias[1:NA, 0:Smax], Bin)
    @variable(model, oaj[0:NA, 1:NO], Bin)

    # Fix some variables
    if NA > 0
        fix(caik[1,1,0], 1, force=true)
        fix(caik[1,2,0], 1, force=true)
        fix(cai[1,1], 1, force=true)
        fix(cai[1,2], 1, force=true)
    end

    @variable(model, 0 <= force_odd[1:NA] <= maximum_value, Int)
    @variable(model, Psias[1:NA, Smin:0], Bin)

    # Warmstart
    if !isempty(addergraph_warmstart)
        set_start_value(ca[0], 1)
        for a in 1:NA
            if get_value(addernodes[a]) > maximum_value
                continue
            end
            left_input, right_input = addernodes_value_to_index[get_input_addernode_values(addernodes[a])[1]], addernodes_value_to_index[get_input_addernode_values(addernodes[a])[2]]
            left_input_value = 1
            if left_input != 0
                left_input_value = get_value(addernodes[left_input])
            end
            right_input_value = 1
            if right_input != 0
                right_input_value = get_value(addernodes[right_input])
            end
            left_shift, right_shift = get_input_shifts(addernodes[a])
            left_negative, right_negative = are_negative_inputs(addernodes[a])
            set_start_value(ca[a], get_value(addernodes[a]))
            set_start_value(ca_no_shift[a], get_value(addernodes[a])*(2^max(-right_shift, 0)))
            set_start_value(force_odd[a], div(get_value(addernodes[a]), 2))
            if left_input_value <= maximum_value-1 && right_input_value <= maximum_value-1
                set_start_value(cai[a, 1], left_input_value)
                set_start_value(cai[a, 2], right_input_value)
                set_start_value(cai_left_sh[a], left_input_value*(2^max(0, left_shift)))
                if left_negative
                    set_start_value(cai_left_shsg[a], -left_input_value*(2^max(0, left_shift)))
                else
                    set_start_value(cai_left_shsg[a], left_input_value*(2^max(0, left_shift)))
                end
                if right_negative
                    set_start_value(cai_right_sg[a], -right_input_value)
                else
                    set_start_value(cai_right_sg[a], right_input_value)
                end
                for k in 0:(a-1)
                    set_start_value(caik[a, 1, k], 0)
                    set_start_value(caik[a, 2, k], 0)
                end
                set_start_value(caik[a, 1, left_input], 1)
                set_start_value(caik[a, 2, right_input], 1)
                set_start_value(Phiai[a, 1], left_negative)
                set_start_value(Phiai[a, 2], right_negative)
                set_start_value.(Psias[a, :], 0)
                set_start_value.(phias[a, :], 0)
                if right_shift >= Smin && right_shift <= 0
                    set_start_value(Psias[a, right_shift], 1)
                end
                if left_shift >= 0 && left_shift <= Smax
                    set_start_value(phias[a, left_shift], 1)
                elseif left_shift < 0
                    set_start_value(phias[a, 0], 1)
                end
            end
            set_start_value.(oaj[a, :], 0)
            if get_value(addernodes[a]) in odd.(abs.(start_values_h))
                set_start_value(oaj[a, findfirst(isequal(get_value(addernodes[a])), odd.(abs.(start_values_h)))], 1)
            end
        end
    end

    # C5.1
    fix(ca[0], 1, force=true)
    # C5.2
    @constraint(model, [a in 1:NA], ca_no_shift[a] == cai_left_shsg[a] + cai_right_sg[a])
    # C5.3
    @constraint(model, [a in 1:NA, s in Smin:0], ca_no_shift[a] >= 2^(-s)*ca[a] + (Psias[a,s] - 1)*(maximum_value*(2^(-s))))
    @constraint(model, [a in 1:NA, s in Smin:0], ca_no_shift[a] <= 2^(-s)*ca[a] + (1 - Psias[a,s])*(maximum_value*(2^(-s))))
    # C5.4
    @constraint(model, [a in 1:NA], sum(Psias[a,s] for s in Smin:0) == 1)
    @constraint(model, [a in 1:NA], sum(Psias[a,s] for s in 0:0) == 1)
    # C5.5
    @constraint(model, [a in 1:NA], phias[a,0] == sum(Psias[a,s] for s in Smin:0)-Psias[a,0])
    # C5.6
    @constraint(model, [a in 1:NA], ca[a] == 2*force_odd[a]+1)
    # C5.7
    @constraint(model, [a in 1:NA, i in 1:2, k in 0:(a-1)], cai[a,i] <= ca[k] + (1-caik[a,i,k])*maximum_value)
    @constraint(model, [a in 1:NA, i in 1:2, k in 0:(a-1)], cai[a,i] >= ca[k] - (1-caik[a,i,k])*maximum_value)
    # C5.8
    @constraint(model, [a in 1:NA, i in 1:2], sum(caik[a,i,k] for k in 0:(a-1)) == 1)
    # C5.9
    @constraint(model, [a in 1:NA, s in 0:Smax], cai_left_sh[a] <= 2^s*cai[a,1] + (1-phias[a,s])*2*maximum_value)
    @constraint(model, [a in 1:NA, s in 0:Smax], cai_left_sh[a] >= 2^s*cai[a,1] - (1-phias[a,s])*(2*maximum_value*(2^s)))
    # C5.10
    @constraint(model, [a in 1:NA], sum(phias[a,s] for s in 0:Smax) == 1)
    # C5.11
    @constraint(model, [a in 1:NA], cai_left_shsg[a] <= -cai_left_sh[a] + (1-Phiai[a,1])*(4*maximum_value))
    @constraint(model, [a in 1:NA], cai_left_shsg[a] >= -cai_left_sh[a] - (1-Phiai[a,1])*2*maximum_value)
    @constraint(model, [a in 1:NA], cai_right_sg[a] <= -cai[a,2] + (1-Phiai[a,2])*(2*maximum_value))
    @constraint(model, [a in 1:NA], cai_right_sg[a] >= -cai[a,2] - (1-Phiai[a,2])*maximum_value)
    # C5.12
    @constraint(model, [a in 1:NA], cai_left_shsg[a] <= cai_left_sh[a] + Phiai[a,1]*2*maximum_value)
    @constraint(model, [a in 1:NA], cai_left_shsg[a] >= cai_left_sh[a] - Phiai[a,1]*(4*maximum_value))
    @constraint(model, [a in 1:NA], cai_right_sg[a] <= cai[a,2] + Phiai[a,2]*maximum_value)
    @constraint(model, [a in 1:NA], cai_right_sg[a] >= cai[a,2] - Phiai[a,2]*(2*maximum_value))
    # C5.13
    @constraint(model, [a in 1:NA], Phiai[a,1] + Phiai[a,2] <= 1)
    # C5.14
    @constraint(model, [a in 0:NA, j in 1:NO], ca[a] <= C[j] + (1-oaj[a,j])*maximum_value)
    @constraint(model, [a in 0:NA, j in 1:NO], ca[a] >= C[j] - (1-oaj[a,j])*maximum_target)
    # C5.15
    @constraint(model, [j in 1:NO], sum(oaj[a,j] for a in 0:NA) == 1 - is_zero_C[j])

    if known_min_NA < NA
        @variable(model, used_adder[(known_min_NA+1):NA], Bin)
        for a in (known_min_NA+1):NA
            set_start_value(used_adder[a], 1)
        end
        @constraint(model, [a in 1:NA, j in 1:NO], oaj[a,j] <= used_adder[a])
        if (known_min_NA+2) <= NA
            # C5.16
            @constraint(model, [a in (known_min_NA+2):NA], used_adder[a] <= used_adder[a-1])
        end
        # C5.17
        @constraint(model, [a in (known_min_NA+1):NA], ca[a] <= used_adder[a]*maximum_value + 1)
        # C5.18
        @constraint(model, [a in (known_min_NA+1):NA, i in 1:2], caik[a,i,0] >= 1-used_adder[a])
    end

    max_wordlength = round(Int, log2((2^wordlength - 1)*(2^wordlength_in - 1)), RoundUp)
    @variable(model, 0 <= Ba[1:NA] <= max_wordlength, Int)
    @variable(model, 0 <= msba[0:NA] <= max_wordlength, Int)
    @variable(model, 0 <= msbai[1:NA, 1:2] <= max_wordlength, Int)
    @variable(model, msbab[1:NA, 1:max_wordlength], Bin)
    @variable(model, 0 <= ga[1:NA] <= max_wordlength-1, Int)
    @variable(model, psia[1:NA], Bin)
    @variable(model, 0 <= shiftal[1:NA] <= Smax, Int)
    @variable(model, Smin <= shifta[1:NA] <= 0, Int)

    @variable(model, 0 <= epsilon_sup[0:NA] <= 2^max_wordlength, Int)
    @variable(model, 0 <= tai[1:NA, 1:2] <= max_wordlength, Int)
    @variable(model, 0 <= tmaxa[1:NA] <= max_wordlength, Int)
    set_start_value.(epsilon_sup[:], 0)

    # C5.19
    @constraint(model, [a in 1:NA], Ba[a] == msba[a] + 1 - ga[a] - psia[a] - shifta[a])
    if known_min_NA < NA
        @variable(model, 0 <= Bau[1:NA] <= max_wordlength, Int)
        @constraint(model, [a in 1:known_min_NA], Bau[a] == Ba[a])
        # C5.20
        @constraint(model, [a in (known_min_NA+1):NA], Bau[a] >= Ba[a] - (1-used_adder[a])*max_wordlength)
        @constraint(model, [a in (known_min_NA+1):NA], Bau[a] <= Ba[a] + (1-used_adder[a])*max_wordlength)
        # C5.21
        @constraint(model, [a in (known_min_NA+1):NA], Bau[a] <= used_adder[a]*max_wordlength)
    end
    # C5.22
    @constraint(model, msba[0] == wordlength_in-1)
    # C5.23
    @constraint(model, [a in 1:NA, i in 1:2, k in 0:(a-1)], msbai[a,i] <= msba[k] + (1-caik[a,i,k])*max_wordlength)
    @constraint(model, [a in 1:NA, i in 1:2, k in 0:(a-1)], msbai[a,i] >= msba[k] - (1-caik[a,i,k])*max_wordlength)
    # C5.24
    @constraint(model, [a in 1:NA], ga[a] <= msba[a])
    # C5.25
    @constraint(model, [a in 1:NA], ga[a] <= tmaxa[a])
    # C5.26
    @constraint(model, [a in 1:NA], ga[a] <= tai[a,1] + max_wordlength*(1-Phiai[a,1]))
    # C5.27
    @constraint(model, [a in 1:NA], ga[a] <= tai[a,2] + max_wordlength*(1-Phiai[a,2]))
    # C5.28
    @constraint(model, [a in 1:NA], shiftal[a] == sum(s*phias[a, s] for s in 1:Smax))
    # C5.29
    @constraint(model, [a in 1:NA], shifta[a] == sum(s*Psias[a, s] for s in Smin:-1))
    # C5.30
    @constraint(model, [a in 1:NA], msbai[a, 1] + shiftal[a] + shifta[a] + 1 <= msba[a] + (max_wordlength+Smax-Smin)*(1-psia[a]))
    # C5.31
    @constraint(model, [a in 1:NA], msbai[a, 2] + shifta[a] + 1 <= msba[a] + (max_wordlength-Smin)*(1-psia[a]))
    # C5.32
    @constraint(model, [a in 1:NA, t in 1:max_wordlength], 2^(t+1)-1 >= (2^wordlength_in-1) * ca[a] + epsilon_sup[a] - (1 - msbab[a, t])*2^max_wordlength)
    # C5.33
    @constraint(model, [a in 1:NA], sum(msbab[a, t] for t in 1:max_wordlength) == 1)
    # C5.34
    @constraint(model, [a in 1:NA], sum(t*msbab[a, t] for t in 1:max_wordlength) == msba[a])

    @variable(model, 0 <= epsilon_infnsha[1:NA] <= 2^max_wordlength, Int)
    @variable(model, 0 <= epsilon_infshai[1:NA, 1:2] <= 2^max_wordlength, Int)
    @variable(model, 0 <= epsilon_inf[0:NA] <= 2^max_wordlength, Int)
    @variable(model, 0 <= epsilon_infai[1:NA, 1:2] <= 2^max_wordlength, Int)
    @variable(model, 0 <= epsilon_supnsha[1:NA] <= 2^max_wordlength, Int)
    @variable(model, 0 <= epsilon_supshai[1:NA, 1:2] <= 2^max_wordlength, Int)
    #@variable(model, 0 <= epsilon_sup[0:NA] <= 2^max_wordlength, Int) # defined above
    @variable(model, 0 <= epsilon_supai[1:NA, 1:2] <= 2^max_wordlength, Int)
    @variable(model, 0 <= epsilon_tai[1:NA, 1:2] <= 2^max_wordlength, Int)
    set_start_value.(epsilon_infnsha[:], 0)
    set_start_value.(epsilon_infshai[:, 1], 0)
    set_start_value.(epsilon_infshai[:, 2], 0)
    set_start_value.(epsilon_inf[:], 0)
    set_start_value.(epsilon_infai[:, 1], 0)
    set_start_value.(epsilon_infai[:, 2], 0)
    set_start_value.(epsilon_supnsha[:], 0)
    set_start_value.(epsilon_supshai[:, 1], 0)
    set_start_value.(epsilon_supshai[:, 2], 0)
    set_start_value.(epsilon_supai[:, 1], 0)
    set_start_value.(epsilon_supai[:, 2], 0)
    set_start_value.(epsilon_tai[:, 1], 0)
    set_start_value.(epsilon_tai[:, 2], 0)

    @variable(model, 0 <= zshai[1:NA, 1:2] <= max_wordlength, Int)
    @variable(model, 0 <= znshai[1:NA, 1:2] <= max_wordlength, Int)
    @variable(model, 0 <= znsha[1:NA] <= max_wordlength, Int)
    @variable(model, 0 <= zai[1:NA, 1:2] <= max_wordlength, Int)
    @variable(model, 0 <= za[0:NA] <= max_wordlength, Int)
    @variable(model, znshBai[1:NA, 1:2], Bin)
    @variable(model, zaib[1:NA, 1:2, 0:max_wordlength], Bin)
    @variable(model, taib[1:NA, 1:2, 0:max_wordlength], Bin)
    @variable(model, 0 <= epsilon_Zai[1:NA, 1:2] <= 2^max_wordlength, Int)
    @variable(model, 0 <= epsilon_Tai[1:NA, 1:2] <= 2^max_wordlength, Int)
    # @variable(model, 0 <= tai[1:NA, 1:2] <= max_wordlength, Int) # defined above
    # @variable(model, 0 <= tmaxa[1:NA] <= max_wordlength, Int) # defined above
    @variable(model, tBa[1:NA], Bin)

    # C5.35
    @constraint(model, [a in 1:NA], epsilon_infnsha[a] >= epsilon_infshai[a,1]+epsilon_tai[a,1]+epsilon_infshai[a,2]+epsilon_tai[a,2] - 4*2^max_wordlength*(Phiai[a,1]+Phiai[a,2]))
    # C5.36
    @constraint(model, [a in 1:NA], epsilon_infnsha[a] >= epsilon_infshai[a,1]+epsilon_tai[a,1]+epsilon_supshai[a,2]+epsilon_tai[a,2] - 4*2^max_wordlength*(1-Phiai[a,2]))
    # C5.37
    @constraint(model, [a in 1:NA], epsilon_infnsha[a] >= epsilon_supshai[a,1]+epsilon_tai[a,1]+epsilon_infshai[a,2]+epsilon_tai[a,2] - 4*2^max_wordlength*(1-Phiai[a,1]))
    # C5.38
    @constraint(model, [a in 1:NA], epsilon_supnsha[a] >= epsilon_supshai[a,1]+epsilon_supshai[a,2] - 2*2^max_wordlength*(Phiai[a,1]+Phiai[a,2]))
    # C5.39
    @constraint(model, [a in 1:NA], epsilon_supnsha[a] >= epsilon_supshai[a,1]+epsilon_infshai[a,2] - 2*2^max_wordlength*(1-Phiai[a,2]))
    # C5.40
    @constraint(model, [a in 1:NA], epsilon_supnsha[a] >= epsilon_infshai[a,1]+epsilon_supshai[a,2] - 2*2^max_wordlength*(1-Phiai[a,1]))
    # C5.41
    @constraint(model, [a in 1:NA, s in Smin:0], epsilon_infnsha[a] >= 2^(-s)*epsilon_inf[a] + (Psias[a,s] - 1)*(maximum_value*(2^(-s))))
    @constraint(model, [a in 1:NA, s in Smin:0], epsilon_infnsha[a] <= 2^(-s)*epsilon_inf[a] + (1 - Psias[a,s])*(maximum_value*(2^(-s))))
    # C5.42
    @constraint(model, [a in 1:NA, s in Smin:0], epsilon_supnsha[a] >= 2^(-s)*epsilon_sup[a] + (Psias[a,s] - 1)*(maximum_value*(2^(-s))))
    @constraint(model, [a in 1:NA, s in Smin:0], epsilon_supnsha[a] <= 2^(-s)*epsilon_sup[a] + (1 - Psias[a,s])*(maximum_value*(2^(-s))))
    # C5.43
    @constraint(model, [a in 1:NA, s in 0:Smax], epsilon_infshai[a,1] <= 2^s*epsilon_inf[a,1] + (1-phias[a,s])*2*maximum_value)
    @constraint(model, [a in 1:NA, s in 0:Smax], epsilon_infshai[a,1] >= 2^s*epsilon_inf[a,1] - (1-phias[a,s])*(2*maximum_value*(2^s)))
    # C5.44
    @constraint(model, [a in 1:NA, s in 0:Smax], epsilon_supshai[a,1] <= 2^s*epsilon_sup[a,1] + (1-phias[a,s])*2*maximum_value)
    @constraint(model, [a in 1:NA, s in 0:Smax], epsilon_supshai[a,1] >= 2^s*epsilon_sup[a,1] - (1-phias[a,s])*(2*maximum_value*(2^s)))
    # C5.45
    @constraint(model, [a in 1:NA], epsilon_infshai[a,2] == epsilon_infai[a,2])
    # C5.46
    @constraint(model, [a in 1:NA], epsilon_supshai[a,2] == epsilon_supai[a,2])
    # C5.47
    @constraint(model, [a in 1:NA, i in 1:2, k in 0:(a-1)], epsilon_infai[a,i] <= epsilon_inf[k] + (1-caik[a,i,k])*maximum_value)
    @constraint(model, [a in 1:NA, i in 1:2, k in 0:(a-1)], epsilon_infai[a,i] >= epsilon_inf[k] - (1-caik[a,i,k])*maximum_value)
    # C5.48
    @constraint(model, [a in 1:NA, i in 1:2, k in 0:(a-1)], epsilon_supai[a,i] <= epsilon_sup[k] + (1-caik[a,i,k])*maximum_value)
    @constraint(model, [a in 1:NA, i in 1:2, k in 0:(a-1)], epsilon_supai[a,i] >= epsilon_sup[k] - (1-caik[a,i,k])*maximum_value)
    # C5.49
    @constraint(model, [a in 1:NA, i in 1:2, k in 0:(a-1)], zai[a,i] <= za[k] + (1-caik[a,i,k])*max_wordlength)
    @constraint(model, [a in 1:NA, i in 1:2, k in 0:(a-1)], zai[a,i] >= za[k] - (1-caik[a,i,k])*max_wordlength)
    # C5.50
    @constraint(model, [a in 1:NA], zshai[a,1] == zai[a,1] + shiftal[a])
    # C5.51
    @constraint(model, [a in 1:NA], zshai[a,2] == zai[a,2])
    # C5.52
    @constraint(model, [a in 1:NA], znshai[a,1] <= zshai[a,1] + max_wordlength*(1-znshBai[a,1]))
    # C5.53
    @constraint(model, [a in 1:NA], znshai[a,1] <= tai[a,1] + max_wordlength*(znshBai[a,1]))
    # C5.54
    @constraint(model, [a in 1:NA], znshai[a,2] <= zshai[a,2] + max_wordlength*(1-znshBai[a,2]))
    # C5.55
    @constraint(model, [a in 1:NA], znshai[a,2] <= tai[a,2] + max_wordlength*(znshBai[a,2]))
    # C5.56
    @constraint(model, [a in 1:NA], znsha[a] <= znshai[a,1])
    # C5.57
    @constraint(model, [a in 1:NA], znsha[a] <= znshai[a,2])
    # C5.58
    @constraint(model, [a in 1:NA], znsha[a] == za[a] + shifta[a])
    # C5.59
    @constraint(model, za[0] == 0)
    # C5.60
    @constraint(model, [a in 1:NA, i in 1:2], sum(taib[a,i,b] for b in 0:max_wordlength) == 1)
    # C5.61
    @constraint(model, [a in 1:NA, i in 1:2], sum(zaib[a,i,b] for b in 0:max_wordlength) == 1)
    # C5.62
    @constraint(model, [a in 1:NA, i in 1:2], sum(b*taib[a,i,b] for b in 0:max_wordlength) == tai[a,i])
    # C5.63
    @constraint(model, [a in 1:NA, i in 1:2], sum(b*zaib[a,i,b] for b in 0:max_wordlength) == zshai[a,i])
    # C5.64
    @constraint(model, [a in 1:NA, i in 1:2, b in 1:max_wordlength], epsilon_Tai[a,i] >= 2^(b-1) - 2^max_wordlength*(1-taib[a,i,b]))
    # C5.65
    #Not necessary
    # C5.66
    @constraint(model, [a in 1:NA, i in 1:2, b in 1:max_wordlength], epsilon_Zai[a,i] <= 2^(b-1) + 2^max_wordlength*(1-zaib[a,i,b]))
    # C5.67
    @constraint(model, [a in 1:NA, i in 1:2], epsilon_Zai[a,i] <= 2^max_wordlength*(1-zaib[a,i,0]))
    # C5.68
    @constraint(model, [a in 1:NA, i in 1:2], epsilon_tai[a,i] >= epsilon_Tai[a,i] - epsilon_Zai[a,i])
    # C5.69
    @constraint(model, [a in 1:NA], epsilon_inf[a] <= errorbound)
    # C5.70
    @constraint(model, [a in 1:NA], epsilon_sup[a] <= errorbound)
    # C5.71
    @constraint(model, epsilon_inf[0] == 0)
    # C5.72
    @constraint(model, epsilon_sup[0] == 0)
    # C5.73
    @constraint(model, [a in 1:NA, i in 1:2], tai[a,i] >= zshai[a,i])
    # C5.74
    @constraint(model, [a in 1:NA], tmaxa[a] <= tai[a,1] + max_wordlength*(1-tBa[a]))
    # C5.75
    @constraint(model, [a in 1:NA], tmaxa[a] <= tai[a,2] + max_wordlength*(tBa[a]))
    # C5.76
    @constraint(model, [a in 1:NA], tai[a,1] <= zshai[a,1] + max_wordlength*(1-Phiai[a,2]))
    # C5.77
    @constraint(model, [a in 1:NA], tai[a,2] <= zshai[a,2] + max_wordlength*(1-Phiai[a,1]))


    # Link errors with outputs
    @variable(model, 0 <= epsilon_abs[0:NA] <= 2^max_wordlength, Int)
    set_start_value.(epsilon_abs[:], 0)
    @constraint(model, [a in 0:NA], epsilon_inf[a] <= epsilon_abs[a])
    @constraint(model, [a in 0:NA], epsilon_sup[a] <= epsilon_abs[a])
    @constraint(model, [a in 0:NA, j in 1:NO], epsilon_abs[a] <= epsilon_C[j] + (1-oaj[a,j])*2^max_wordlength)
    @constraint(model, [a in 0:NA, j in 1:NO], epsilon_abs[a] >= epsilon_C[j] - (1-oaj[a,j])*2^max_wordlength)

    if !simplify_cost
        @constraint(model, [a in 0:NA, j in 1:NO], msba[a] <= msb_C[j] + (1-oaj[a,j])*max_wordlength)
        @constraint(model, [a in 0:NA, j in 1:NO], msba[a] >= msb_C[j] - (1-oaj[a,j])*max_wordlength)

        @constraint(model, [a in 0:NA, j in 1:NO], za[a] <= zeros_C[j] + (1-oaj[a,j])*max_wordlength)
        @constraint(model, [a in 0:NA, j in 1:NO], za[a] >= zeros_C[j] - (1-oaj[a,j])*max_wordlength)
    end

    # O5
    if known_min_NA < NA
        @objective(model, Min, sum(Bau) + sum(oba_C))
    else
        @objective(model, Min, sum(Ba) + sum(oba_C))
    end

    return model
end




"""
    optimize_increment!(model::Model, model_mcm_forumlation!::Function,
                        C::Vector{Int}, wordlength::Int, S::Tuple{Int, Int},
                        verbose::Bool)::Model

Increment NA until a solution is found for the coefficients in `C`.
"""
function optimize_increment!(model::Model,
                             wordlength::Int,
                             specifications::Vector{Tuple{Float64, Float64, Float64}},
                             bounds_C::Vector{Tuple{Int, Int}},
                             start_values_h::Vector{Int}=Vector{Int}(),
                             ;verbose::Bool = false, nb_adders_lb::Int = 0,
                             verbose_solver::Bool = false,
                             use_warmstart::Bool=true,
                             kwargs...)::Model
    NA = nb_adders_lb
    empty!(model)
    fir_design!(model, specifications, wordlength; verbose=verbose, kwargs...)
    C = model[:ToLink]
    is_zero_C = model[:ToLinkIsZero]
    epsilon_C = model[:ToLinkError]
    msb_C = model[:ToLinkMSB]
    zeros_C = model[:ToLinkZeros]
    oba_C = model[:ToLinkOBA]
    fix_to_bounds!(model, bounds_C)
    addergraph_warmstart = AdderGraph()
    fix_start!(model, start_values_h; kwargs...)
    if use_warmstart
        addergraph_warmstart = rpag(filter!(x -> x > 1, unique!(odd.(abs.(start_values_h)))))
        NA = length(get_nodes(addergraph_warmstart))
    end
    model_mcm_forumlation!(
        model, C, is_zero_C, epsilon_C, msb_C, zeros_C, oba_C, wordlength, NA;
        start_values_h=start_values_h,
        addergraph_warmstart=addergraph_warmstart,
        verbose=verbose, kwargs...
    )
    timelimit = time_limit_sec(model)
    total_solve_time = 0.0
    if !verbose_solver
        set_silent(model)
    else
        unset_silent(model)
    end
    optimize!(model)
    current_solve_time = solve_time(model)
    total_solve_time += current_solve_time
    while termination_status(model) in [MOI.INFEASIBLE, MOI.INFEASIBLE_OR_UNBOUNDED]
        if !isnothing(timelimit)
            timelimit -= current_solve_time
        end
        verbose && println("$(termination_status(model)) for NA = $NA in $current_solve_time seconds")
        if !isnothing(timelimit)
            if timelimit <= 0.0
                break
            end
        end
        NA += 1
        empty!(model)
        fir_design!(model, specifications, wordlength; verbose=verbose, kwargs...)
        fix_to_bounds!(model, bounds_C)
        fix_start!(model, start_values_h; kwargs...)
        C = model[:ToLink]
        is_zero_C = model[:ToLinkIsZero]
        epsilon_C = model[:ToLinkError]
        msb_C = model[:ToLinkMSB]
        zeros_C = model[:ToLinkZeros]
        oba_C = model[:ToLinkOBA]

        model_mcm_forumlation!(
            model, C, is_zero_C, epsilon_C, msb_C, zeros_C, oba_C, wordlength, NA;
            start_values_h=start_values_h,
            known_min_NA=NA, verbose=verbose, kwargs...
        )
        if !isnothing(timelimit)
            set_time_limit_sec(model, timelimit)
        end
        optimize!(model)
        current_solve_time = solve_time(model)
        total_solve_time += current_solve_time
    end
    model[:NA] = Vector{Int}()
    count_solution = 0
    while has_values(model; result=count_solution+1)
        count_solution += 1
        if NA == 0
            push!(model[:NA], NA)
        else
            push!(model[:NA], sum(round(value(model[:ca][a])) != 1 ? 1 : 0 for a in 1:NA))
        end
    end
    if !isempty(model[:NA])
        NA = model[:NA][1]
    end
    verbose && println("\n$(termination_status(model)) for NA = $NA in $current_solve_time seconds\nTotal time: $total_solve_time seconds\n\n\n")

    return model
end
