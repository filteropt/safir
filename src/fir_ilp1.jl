function fir_ilp1!(model::Model,
                   wordlength::Int,
                   specifications::Vector{Tuple{Float64, Float64, Float64}},
                   ;
                   verbose::Bool = false,
                   filter_type::Int,
                   filter_order::Int = 0,
                   debug::Bool = false,
                   find_filter_order::Bool = false,
                   wordlength_in::Int = wordlength_in,
                   kwargs...
    )
    solution = Fir2AdderGraphs()

    firstwhile = true
    if filter_order == 0
        find_filter_order = true
        filter_order = mod(filter_type, 2) + 1
    end

    infeasibility_proven = false
    while firstwhile || (find_filter_order && infeasibility_proven)
        if !firstwhile
            filter_order += 2
        end
        firstwhile = false
        # Call fir_design!
        verbose && println("Generation of FIR design part, filter_order: $(filter_order)")
        empty!(model)
        fir_design!(
            model, specifications, wordlength;
            filter_order=filter_order, filter_type=filter_type,
            verbose=verbose, wordlength_in=wordlength_in,
            step_presolve=true, kwargs...
        )

        presolve_done = true
        verbose && println("Presolving")
        presolve!(model; verbose=verbose, kwargs...)
        infeasibility_proven = model[:infeasibility_proven]
    end

    if !infeasibility_proven
        # Add MCM ILP1 to model
        verbose && println("Adding MCM to the model and solving...")
        optimize_increment!(model, wordlength, specifications,
                            copy(model[:bounds]),
                            copy(model[:start_values_h]),
                            ;
                            verbose=verbose,
                            filter_order=filter_order,
                            filter_type=filter_type,
                            wordlength_in=wordlength_in,
                            kwargs...)

        # if termination_status(model) == MOI.OPTIMAL
        if has_values(model)
            solution.coefficients = Vector{Int}()
            solution.addergraph = AdderGraph()
            current_result = 1
            not_valid = true
            while not_valid && has_values(model; result=current_result)
                NA = model[:NA][current_result]
                best_A = NA#+sum((round(Int, value(model[:h][j])) != 0) for j in 0:(length(model[:h])-1))-1
                verbose && println("Current solution -- A: $(best_A)")
                solution.coefficients = Vector{Int}()
                hcoefficients = collect(round.(Int, value.(model[:h]; result = current_result)))
                if filter_type == 1
                    append!(solution.coefficients, reverse(hcoefficients[2:end]))
                elseif filter_type == 2
                    append!(solution.coefficients, reverse(hcoefficients))
                elseif filter_type == 3
                    append!(solution.coefficients, -reverse(hcoefficients))
                    push!(solution.coefficients, 0)
                elseif filter_type == 4
                    append!(solution.coefficients, -reverse(hcoefficients))
                end
                append!(solution.coefficients, hcoefficients)
                verbose && println("\tcoefficients: ", solution.coefficients)
                solution.gain = value.(model[:gain]; result = current_result)
                verbose && println("\tgain: ", solution.gain)

                solution.addergraph = AdderGraph(filter!(x -> x >= 1, unique!(abs.(collect(solution.coefficients)))))
                for i in 1:model[:NA][current_result]
                    node_shift = 0
                    for s in -wordlength:0
                        if round(Int, value(model[:Psias][i,s]; result = current_result)) == 1
                            node_shift = s
                            break
                        end
                    end
                    input_shift = 0
                    for s in 0:wordlength
                        if round(Int, value(model[:phias][i,s]; result = current_result)) == 1
                            input_shift = s
                            break
                        end
                    end
                    subtraction = [value(model[:cai_left_shsg][i]; result = current_result) < 0, value(model[:cai_right_sg][i]; result = current_result) < 0]
                    truncateleft = max(round(Int, value(model[:tai][i,1]; result = current_result)-input_shift+node_shift), 0)
                    truncateright = max(round(Int, value(model[:tai][i,2]; result = current_result)+node_shift), 0)
                    left_addernode = InputEdge(get_origin(solution.addergraph), input_shift+node_shift, subtraction[1], truncateleft)
                    right_addernode = InputEdge(get_origin(solution.addergraph), node_shift, subtraction[2], truncateright)
                    if !isempty(get_addernodes_by_value(solution.addergraph, round(Int, value(model[:cai][i,1]; result = current_result)))) && !isempty(get_addernodes_by_value(solution.addergraph, round(Int, value(model[:cai][i,2]; result = current_result))))
                        left_addernode = InputEdge(get_addernodes_by_value(solution.addergraph, round(Int, value(model[:cai][i,1]; result = current_result)))[end], input_shift+node_shift, subtraction[1], truncateleft)
                        right_addernode = InputEdge(get_addernodes_by_value(solution.addergraph, round(Int, value(model[:cai][i,2]; result = current_result)))[end], node_shift, subtraction[2], truncateright)
                    end
                    push_node!(solution.addergraph,
                        AdderNode(round(Int, value(model[:ca][i]; result = current_result)),
                            [left_addernode, right_addernode]
                        )
                    )
                    println(round(Int, value(model[:ca][i]; result = current_result)))
                end
                println(solution.coefficients)

                if debug
                    println()
                    print("./flopoco verbose=2 frequency=1 FixFIRTransposed ")
                    print("Win=$(wordlength_in) coeff=$(join(solution.coefficients, ":")) ")
                    print("$(replace(write_addergraph(solution.addergraph), " " => "")) ")
                    print("graph_$(replace(replace(write_addergraph_truncations(solution.addergraph), " " => ""), ";" => "|")) ")
                    print("sa_truncations=\"$(replace(replace(replace(join(solution.truncations, "|"), " " => ""), ")" => ""), "(" => ""))\" ")
                    print("Testbench n=1000")
                    println()
                end

                not_valid = !isvalid(solution.addergraph)
                if not_valid
                    solution.addergraph = AdderGraph()
                else
                    model[:valid_objective_value] = objective_value(model; result = current_result)
                end
                current_result = current_result + 1
            end
            if not_valid
                @warn "Could not provide a valid adder graph"
            else
                current_result = current_result - 1
                if debug
                    print("Errors: ")
                    print("$(round(Int, value(model[:error_out_structadder][1])))")
                    for j in 3:length(solution.coefficients)
                        print(", $(round(Int, value(model[:error_out_structadder][j-1])))")
                    end
                    println()

                    print("Errors in: ")
                    print("($(round(Int, value(model[:error_in_structadder][1, 1]))), ")
                    print("$(round(Int, value(model[:error_in_structadder][1, 2]))))")
                    for j in 3:length(solution.coefficients)
                        print(", ($(round(Int, value(model[:error_in_structadder][j-1, 1]))), ")
                        print("$(round(Int, value(model[:error_in_structadder][j-1, 2]))))")
                    end
                    println()

                    print("Error truncations: ")
                    print("($(round(Int, value(model[:error_ti_structadder][1, 1]))), ")
                    print("$(round(Int, value(model[:error_ti_structadder][1, 2]))))")
                    for j in 3:length(solution.coefficients)
                        print(", ($(round(Int, value(model[:error_ti_structadder][j-1, 1]))), ")
                        print("$(round(Int, value(model[:error_ti_structadder][j-1, 2]))))")
                    end
                    println()

                    print("Error truncations max: ")
                    print("($(round(Int, value(model[:error_Ti_structadder][1, 1]))), ")
                    print("$(round(Int, value(model[:error_Ti_structadder][1, 2]))))")
                    for j in 3:length(solution.coefficients)
                        print(", ($(round(Int, value(model[:error_Ti_structadder][j-1, 1]))), ")
                        print("$(round(Int, value(model[:error_Ti_structadder][j-1, 2]))))")
                    end
                    println()


                    push!(solution.truncations, (0, 0))
                    print("$(solution.truncations[end])")
                    for j in 2:length(solution.coefficients)
                        push!(solution.truncations, (round(Int, value(model[:ti_structadder][j-1, 1])), round(Int, value(model[:ti_structadder][j-1, 2]))))
                        print(", $(solution.truncations[end])")
                    end
                    println()


                    print("Save zeros: ")
                    print("($(round(Int, value(model[:error_Zi_structadder][1, 1]))), ")
                    print("$(round(Int, value(model[:error_Zi_structadder][1, 2]))))")
                    for j in 3:length(solution.coefficients)
                        print(", ($(round(Int, value(model[:error_Zi_structadder][j-1, 1]))), ")
                        print("$(round(Int, value(model[:error_Zi_structadder][j-1, 2]))))")
                    end
                    println()

                    print("Nb zeros: ")
                    print("($(round(Int, value(model[:nbzeros_in_structadder][1, 1]))), ")
                    print("$(round(Int, value(model[:nbzeros_in_structadder][1, 2]))))")
                    for j in 3:length(solution.coefficients)
                        print(", ($(round(Int, value(model[:nbzeros_in_structadder][j-1, 1]))), ")
                        print("$(round(Int, value(model[:nbzeros_in_structadder][j-1, 2]))))")
                    end
                    println()


                    println("nbzeros_outputs_shifted: $(round.(Int, collect(value.(model[:nbzeros_outputs_shifted]; result = current_result))))")

                    println("Hbound: $(round.(Int, collect(value.(model[:Hbound]; result = current_result))))")

                    println("ToLinkMSB: $(round.(Int, collect(value.(model[:ToLinkMSB]; result = current_result))))")
                    println("msb_out_structadder: $(round.(Int, collect(value.(model[:msb_out_structadder]; result = current_result))))")
                    println("msb_in_structadder 1: $(round.(Int, collect(value.(model[:msb_in_structadder][:, 1]; result = current_result))))")
                    println("msb_in_structadder 2: $(round.(Int, collect(value.(model[:msb_in_structadder][:, 2]; result = current_result))))")

                    println("ToLinkZeros: $(round.(Int, collect(value.(model[:ToLinkZeros]; result = current_result))))")
                    println("ToLinkOBA: $(round.(Int, collect(value.(model[:ToLinkOBA]; result = current_result))))")
                end

                verbose && println("Number of one-bit adders: $(round(Int, objective_value(model; result = current_result)))")
                verbose && println("Number of one-bit adders in AG: $(compute_total_nb_onebit_adders(solution.addergraph, wordlength_in))")
                verbose && println("Number of one-bit adders in struct: $(round(Int, objective_value(model; result = current_result)) - compute_total_nb_onebit_adders(solution.addergraph, wordlength_in))")
            end
        end
    else
        @warn "No FIR filter for these specifications and filter type/filter order"
    end

    return solution
end
