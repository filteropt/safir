function design_fir(
        model::Model,
        specifications::Vector{Tuple{Float64, Float64, Float64}},
        wordlength::Int;
        use_big_m::Bool=true,
        verbose::Bool=false, debug::Bool=true,
        kwargs...
    )
    tmp_time = @timed begin
    if use_big_m && wordlength >= 10
        @warn "Default integrality tolerance might lead to numerical instability"
    end
    solution = fir_ilp1!(model, wordlength, specifications; use_big_m=use_big_m, verbose=verbose, debug=debug, kwargs...)
    end #@timed
    verbose && println("Solving time: $(tmp_time[2])")

    return solution
end

design_fir(optimizer::DataType, args...; kwargs...) = design_fir(Model(optimizer), args...; kwargs...)

design_fir(model::Model,
                        fbands::Vector{Tuple{Float64, Float64}},
                        abands::Vector{Tuple{Float64, Float64}},
                        wordlength::Int;
                        size_of_grid::Int=480,
                        kwargs...
    ) = design_fir(model, get_specifications(fbands, abands, size_of_grid), wordlength; kwargs...)

design_fir(model::Model,
                        fbands::Vector{Tuple{Float64, Float64}},
                        dbands::Union{Vector{Float64},Vector{Int}},
                        deltas::Vector{Float64},
                        wordlength::Int;
                        size_of_grid::Int=480,
                        kwargs...
    ) = design_fir(model, get_specifications(fbands, dbands, deltas, size_of_grid), wordlength; kwargs...)

design_fir(model::Model,
                        fbands::Vector{Tuple{Float64, Float64}},
                        dbands::Union{Vector{Float64},Vector{Int}},
                        delta::Float64,
                        wordlength::Int;
                        size_of_grid::Int=480,
                        kwargs...
    ) = design_fir(model, get_specifications(fbands, dbands, delta, size_of_grid), wordlength; kwargs...)

design_fir(model::Model,
                        transfer_function::Function,
                        error_margin_percent::Union{Float64, Int},
                        error_minimum::Float64,
                        wordlength::Int;
                        size_of_grid::Int=480,
                        kwargs...
    ) = design_fir(model, get_specifications(transfer_function, error_margin_percent, error_minimum, size_of_grid), wordlength; kwargs...)
