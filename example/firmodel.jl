using JuMP
using Gurobi
using AdderGraphs

include("$(@__DIR__())/../src/jFIR2AG.jl")

using .jFIR2AG



function main(arg_errorbound::Int)
    errorbound = 0
    if arg_errorbound >= 0
        errorbound = 2^arg_errorbound-1
    end

    # ---------------------- Toy example ---------------------- #

    wordlength = 7
    wordlength_in = 8
    model = Model(Gurobi.Optimizer)
    set_optimizer_attributes(model, "Threads" => 4)
    verbose = true
    verbose_solver = false
    verbose_presolve = false
    filter_type = 1
    fixed_gain = true
    filter_order = 8
    use_warmstart = false
    set_time_limit_sec(model, 24*60*60)

    specifications = get_specifications(
        Vector{Tuple{Float64, Float64}}([(0.0, 0.3),(0.5, 1.0)]),
        Vector{Int}([1,0]),
        0.1,
        480
    )

    solution = design_fir(model,
        specifications,
        wordlength,
        filter_type=filter_type,
        verbose=verbose,
        verbose_solver=verbose_solver,
        fixed_gain=fixed_gain,
        filter_order=filter_order,
        verbose_presolve=verbose_presolve,
        wordlength_in=wordlength_in,
        errorbound=errorbound,
        use_warmstart=use_warmstart,
    )
    println("Coefficients: $(solution.coefficients)")
    println("Truncations: $(solution.truncations)")
    println(write_addergraph(solution.addergraph))
    println(write_addergraph_truncations(solution.addergraph))
    println("Number of adders: $(length(get_nodes(solution.addergraph)))")
    println("Number of one-bit adders: $(compute_total_nb_onebit_adders(solution.addergraph, wordlength_in))")

    println()
    print("./flopoco verbose=2 frequency=1 FixFIRTransposed ")
    print("Win=$(wordlength_in) coeff=$(join(solution.coefficients, ":")) ")
    print("$(replace(write_addergraph(solution.addergraph), " " => "")) ")
    print("graph_$(replace(replace(write_addergraph_truncations(solution.addergraph), " " => ""), ";" => "|")) ")
    print("sa_truncations=\"$(replace(replace(replace(join(solution.truncations, "|"), " " => ""), ")" => ""), "(" => ""))\" ")
    print("Testbench n=1000")
    println()

    println("\n\n\n\n\n\n\n\n\n\n")

    return solution
    # return nothing
end

