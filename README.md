# SAFIR 
**Shift-And-Add circuits for digital FIR filters**

This git was created in a work to design digital FIR filters using shift-and-add from frequency and accuracy specification with a minimum number of full/half adders.
Integer Linear Programming was used to solve this problem.
For more details see:

Anastasia Volkova, Rémi Garcia, Florent de Dinechin, Martin Kumm, Hardware-optimal digital FIR filters: one ILP to rule them all and in faithfulness bind them, Asilomar Conference on Signals, Systems and Computers, 2023



**Examples from the Asilomar paper**

To optimize the toy example filter T, call

```
julia firmodel.jl Toy n
```

where the error is guaranteed to be less than epsilon=2^n.
In the paper we used n=-1, 4 and 8 (-1 corresponds to epsilon=0).
For filter "S2", just replace "T" by "S2".

The output of the optimization is the number of required one-bit adders(in addder graph, structural adders and total), the coefficients, the adder graph and the truncations.
Finally, it generates a call to [FloPoCo](https://flopoco.org), to produce the VHDL code of the filter and a test bench containing all the necessary information (see the command line help of FloPoCo for explanations of the format).

For example, the toy example of Fig. 4 in the paper can be obtained by 

```
julia firmodel.jl Toy 6
```

leading to the following FloPoCo call:

```
./flopoco verbose=2 frequency=1 IntFIRTransposed Win=8 coeff=-4:-4:5:20:24:20:5:-4:-4 graph="{{'O',[4],1,[1],0,2},{'O',[5],1,[5],1,0},{'O',[20],1,[5],1,2},{'O',[24],1,[3],1,3},{'A',[3],1,[1],0,1,[1],0,0},{'A',[5],1,[1],0,2,[1],0,0}}" graph_truncations="3,1:0,0|5,1:0,0" sa_truncations="0,0|2,2|4,0|0,4|0,4|0,4|4,0|0,2|0,2" epsilon=64 Testbench n=1000
```
The test bench can be directly performed by running the [nvc](https://github.com/nickg/nvc) command line as given by FloPoCo.


Specifiying the output word size "wOut" will add the final rounding (when wOut is less than maximally required). This is the call used for synthesis:

```
./flopoco verbose=2 frequency=1 IntFIRTransposed Win=8 coeff=-4:-4:5:20:24:20:5:-4:-4 graph="{{'O',[4],1,[1],0,2},{'O',[5],1,[5],1,0},{'O',[20],1,[5],1,2},{'O',[24],1,[3],1,3},{'A',[3],1,[1],0,1,[1],0,0},{'A',[5],1,[1],0,2,[1],0,0}}" graph_truncations="3,1:0,0|5,1:0,0" sa_truncations="0,0|2,2|4,0|0,4|0,4|0,4|4,0|0,2|0,2" wOut=8
```


